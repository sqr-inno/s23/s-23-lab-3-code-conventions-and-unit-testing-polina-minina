package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import com.hw.db.models.Thread;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Timestamp;
import java.util.List;

class ThreadControllerTests {
    private ThreadController threadController;
    private Thread thread;
    private Thread thread2;
    private User user;
    private Post post;

    String author = "Bob Mill";
    String author2 = "Job Mill";
    String forum = "SomeForum";
    String message = "SomeMessage";
    int parent = 0;
    int threadInt = 0;

    String email = "bob_mill@pochta.com";
    String about = "Bob Mill About Example";

    String slug = "SomeSlug";
    String slug2 = "SomeSlug2";
    String title = "SomeTitle";
    int votes = 0;

    int threadId = 1;
    int threadId2 = 2;

    @BeforeEach
    @DisplayName("thread creation test")
    void createThreadTest() {
        threadController = new ThreadController();
        post = new Post(author, new Timestamp(123L), forum, message, parent, threadInt, false);
        user = new User(author, email, author, about);

        thread = new Thread(author, new Timestamp(123L), forum, message, slug, title, votes);
        thread.setId(threadId);

        thread2 = new Thread(author2, new Timestamp(123L), forum, message + " 2", slug2, title + " 2", votes);
        thread2.setId(threadId2);
    }

    @Test
    @DisplayName("CheckIdOrSlug test id")
    void testCheckId() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(threadId)).thenReturn(thread);
            assertEquals(thread, threadController.CheckIdOrSlug(threadId + ""));
        }
    }

    @Test
    @DisplayName("CheckIdOrSlug test slug")
    void testCheckSlug() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
            assertEquals(thread, threadController.CheckIdOrSlug(slug));
        }
    }

    @Test
    @DisplayName("createPost test")
    void testCreatePost() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                threadMock.when(() -> ThreadDAO.getThreadById(threadId)).thenReturn(thread);
                threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                userMock.when(() -> UserDAO.Info(author)).thenReturn(user);
                ResponseEntity resCreatePosts = threadController.createPost(slug, List.of(post));
                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(List.of(post)), resCreatePosts);
            }
        }
    }

    @Test
    @DisplayName("Posts method test")
    void testPosts() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                userMock.when(() -> UserDAO.Info(author)).thenReturn(user);
                threadMock.when(() -> ThreadDAO.getThreadById(threadId)).thenReturn(thread);
                threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                threadMock.when(() -> ThreadDAO.getPosts(threadId, 1, 0, "asc", true)).thenReturn(List.of(post));
                ResponseEntity resPosts = threadController.Posts(slug, 1, 0, "asc", true);
                assertEquals(ResponseEntity.status(HttpStatus.OK).body(List.of(post)), resPosts);
            }
        }
    }

    @Test
    @DisplayName("change method test")
    void testChange() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(threadId)).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadById(threadId2)).thenReturn(thread2);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug2)).thenReturn(thread2);
            ResponseEntity resChange = threadController.change(slug2, thread2);
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread2), resChange);
        }
    }

    @Test
    @DisplayName("info method test")
    void testInfo() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(threadId)).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
            ResponseEntity resInfo = threadController.info(slug);
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), resInfo);
        }
    }

    @Test
    @DisplayName("createVote method test")
    void testCreateVote() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                userMock.when(() -> UserDAO.Info(author)).thenReturn(user);
                threadMock.when(() -> ThreadDAO.getThreadById(threadId)).thenReturn(thread);
                threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                threadMock.when(() -> ThreadDAO.change(new Vote(author, 1), thread.getVotes()))
                        .thenReturn(thread.getVotes());
                int votesStart = thread.getVotes();
                ResponseEntity resVote = threadController.createVote(slug, new Vote(author, 11));
                int votesEnd = thread.getVotes();
                assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), resVote);
                assertEquals(11, votesEnd - votesStart);
            }
        }
    }
}


